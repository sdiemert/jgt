package com.sdiemert.jgt.fs;

import com.sdiemert.jgt.core.*;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.activation.UnsupportedDataTypeException;
import java.io.IOException;
import java.util.ArrayList;

public class JSONFileWriter extends FileWriter {

    /**
     * Writes the provided Graph to a JSON file.
     * @param g the Graph object to write.
     * @param path the path to write the object to, will overwrite existing files at this location.
     * @throws IOException if the file cannot be opened or written.
     */
    public void writeGraph(Graph g, String path) throws IOException{
        toFile(path, graphToJSON(g).toString());
    }

    /**
     * Writes the provided GTSystem to a JSON file.
     * @param s the GTSystem to write.
     * @param path the path to write the object to, will overwrite existing files.
     * @throws IOException if the file cannot be opened or written.
     */
    public void writeSystem(GTSystem s, String path) throws IOException{

        JSONObject jS = new JSONObject();
        jS.put("id", s.getId());

        JSONArray rules = new JSONArray();

        for(Rule r : s.getRules()){
            rules.put(ruleToJSON(r));
        }

        jS.put("rules", rules);

        toFile(path, jS.toString());
    }

    JSONObject ruleToJSON(Rule r) {
        JSONObject obj = new JSONObject();
        obj.put("id", r.getId());
        obj.put("graph", graphToJSON(r.getRuleGraph()));
        obj.put("nacs", nacsToJSON(r.getNacs()));

        JSONArray arr;

        arr = new JSONArray();
        for(Node n : r.getAddNodes()){
            arr.put(n.getId());
        }
        obj.put("addNodes", arr);

        arr = new JSONArray();
        for(Edge e : r.getAddEdges()){
            arr.put(e.getId());
        }
        obj.put("addEdges", arr);

        arr = new JSONArray();
        for(Node n : r.getDelNodes()){
            arr.put(n.getId());
        }
        obj.put("delNodes", arr);

        arr = new JSONArray();
        for(Edge e : r.getDelEdges()){
            arr.put(e.getId());
        }
        obj.put("delEdges", arr);

        return obj;
    }

    JSONArray nacsToJSON(ArrayList<NAC> nacs) {

        JSONArray ret = new JSONArray();

        JSONObject tmp, tmp2;

        for(NAC n : nacs){

            tmp = new JSONObject();

            switch(n.getNACType()){

                case SINGLE_NODE:
                    tmp.put("node", n.getNACSrc().getLabel());
                    ret.put(tmp);
                    break;
                case SINGLE_EDGE:
                    tmp.put("edge", n.getNACEdge().getLabel());
                    tmp.put("src", n.getNACEdge().getSrc().getId());
                    tmp.put("tar", n.getNACEdge().getTar().getId());
                    ret.put(tmp);
                    break;

                case SRC_EDGE:
                    tmp.put("edge", n.getNACEdge().getLabel());
                    tmp.put("tar", n.getNACEdge().getTar().getId());
                    // holds the NAC SRC node label.
                    tmp2 = new JSONObject();
                    tmp2.put("label", n.getNACSrc().getLabel());
                    tmp.put("src", tmp2);
                    ret.put(tmp);
                    break;
                case EDGE_TAR:
                    tmp.put("edge", n.getNACEdge().getLabel());
                    tmp.put("src", n.getNACEdge().getSrc().getId());
                    // holds the NAC TAR node label.
                    tmp2 = new JSONObject();
                    tmp2.put("label", n.getNACTar().getLabel());
                    tmp.put("tar", tmp2);
                    ret.put(tmp);
                    break;
                default:
                    break;
            }

        }

        return ret;
    }

    JSONObject graphToJSON(Graph g) {

        JSONObject jG = new JSONObject();

        jG.put("id", g.getId());

        JSONArray nodes = new JSONArray();
        JSONArray edges = new JSONArray();

        for(Node n : g.getNodes()){
            nodes.put(nodeToJSON(n));
        }

        for(Edge e : g.getEdges()){
            edges.put(edgeToJSON(e));
        }

        jG.put("nodes", nodes);
        jG.put("edges", edges);

        return jG;
    }

    JSONObject nodeToJSON(Node n) {
        JSONObject j = new JSONObject();
        j.put("id", n.getId());
        j.put("label", n.getLabel());

        if(n.getData() != null){

            if(n.getData() instanceof IntNodeData){
                j.put("data", ((IntNodeData) n.getData()).getVal());
            }else if(n.getData() instanceof StringNodeData){
                j.put("data", ((StringNodeData) n.getData()).getVal());
            }else{
                j.put("data", n.getData().toString());
            }
        }

        return j;
    }

    JSONObject edgeToJSON(Edge e){
        JSONObject j = new JSONObject();
        j.put("id", e.getId());
        j.put("src", e.getSrc().getId());
        j.put("tar", e.getTar().getId());
        j.put("label", e.getLabel());
        return j;
    }


}
