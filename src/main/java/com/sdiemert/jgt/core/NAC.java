package com.sdiemert.jgt.core;

import static com.sdiemert.jgt.core.NAC.NACType.NONE;

public class NAC{

    private Node src = null;
    private Node tar = null;
    private Edge edge = null;

    /**
     * Creates a new NAC object that will prevent matching any sub-graph of the host
     * that contains a corresponding node.
     *
     * Rule Summary: (XA)
     *
     * @param nodeNac the NAC node to prevent matching against.
     */
    public NAC(Node nodeNac) throws RuleException {
        if(nodeNac == null) throw new RuleException("nacNode must not be null.");
        this.src = nodeNac;
    }

    public NAC(Edge edgeNac) throws RuleException{
        this.edge = edgeNac;
        if(this.edge == null) throw new RuleException("nacEdge must not be null.");
    }

    public NAC(Node srcNac, Edge edgeNac) throws RuleException {

        this.src = srcNac;
        this.edge = edgeNac;

        if(this.src == null) throw new RuleException("srcNac must not be null.");
        else if(this.edge == null) throw new RuleException("edgeNac must not be null.");
        else if(!this.edge.getSrc().equals(srcNac))throw new RuleException("srcNac must match edgeNac.src.");
    }

    public NAC(Edge edgeNac, Node tarNac) throws RuleException {
        this.tar = tarNac;
        this.edge = edgeNac;

        if(this.tar == null) throw new RuleException("tarNac must not be null.");
        if(this.edge == null) throw new RuleException("edgeNac must not be null.");
        else if(!this.edge.getTar().equals(tarNac))throw new RuleException("tarNac must match edgeNac.tar");
    }

    public Node getNACSrc() {
        return src;
    }

    public Node getNACTar() {
        return tar;
    }

    public Edge getNACEdge() {
        return edge;
    }

    public NACType getNACType() {
        if(this.src != null && this.edge == null && this.tar == null) return NACType.SINGLE_NODE;
        else if(this.edge != null && this.src == null && this.tar == null)  return NACType.SINGLE_EDGE;
        else if(this.src != null && this.edge != null && this.tar == null)  return NACType.SRC_EDGE;
        else if(this.tar != null && this.edge != null && this.src == null)  return NACType.EDGE_TAR;
        else return NONE;
    }

    public enum NACType{
        SINGLE_NODE, // (XA) - there is not match node with label A.
        SINGLE_EDGE, // (A)-XE->(B)
        SRC_EDGE, // (XA)-XE->(B)
        EDGE_TAR, // (A)-XE->(XB)
        NONE // error - this should not be possible given the class invariant...
    }

}
