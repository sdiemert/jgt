# jGT

jGT is a graph transformation library written in Java that leverages the [Z3 SMT Solver](https://github.com/z3prover).

This project is licensed under the MIT License. 

This project was previously on Github at: [https://github.com/sdiemert/jgt](https://github.com/sdiemert/jgt) but moved to Gitlab in December 2018. 

## About jGT

### What is Graph Transformation?

Graph Grammers (GGs) are generalization of string grammers. A GG is a collection of rules that specify changes to a given input graph. Graph Transformation (GT) systems include graph grammars and a starting host graph to which the rules are applied. GT systems provide a paradigm for formally specifying changes to graphs. A sub-field of Computer Science is dedicated to the study of graph transformation. 

Learn more about Graph Transformation with jGT on the [project Wiki page (TBD)](#). 

### Why jGT?

The world of graph transformation is theoretically very rich. Steady progress has been made in this field since its conception in 1960s. However, to date software support for Graph Transformation is limited. Many acaedemic groups have developed tools for specifying and verifying GT systems. These are avaliable but (with a few exceptions) they fall into two categories: 

1. They are old/out-dated and do not run on modern platforms. 
2. They are rely heavily on external environments (e.g., Eclipse tools).

Additionally, it is not clear how existing tools provide library support for use within other software projects. 

jGT aims to address this gap by providing an easily integrated graph transformation library that can be incorporated into any Java project (provided it can use the Z3 prover).

### What is Unique about jGT? 

Aside from being a library for GT in Java, jGT is also theoretically unique as it leverages an SMT solver (Z3) to perform the "hard work" of graph matching. This results in a performant run-time library that can be used in other Java projects requiring formally specified graph operations. 

## Setup and Usage

### Setup

jGT is bundled as a Java JAR file, the for the latest release can be obtained from: 

* [Weekly Gitlab Build of Master](https://gitlab.com/sdiemert/jgt/-/jobs/artifacts/master/download?job=makejarscheduled)
* Releases:
    + [jGT Hydrogen 1.1](https://gitlab.com/sdiemert/jgt/-/jobs/artifacts/1.1-jgt-hydrogen/download?job=makejarstag)

jGT's API is accessible to a normal Java program provided the following conditions are met: 

1. The `jgt-deps-x.x.jar` (the library + Java dependencies bundled into a single jar file) is accessible in your classpath. 
2. The Z3 native libraries are acessible to Java via: 
    + a) `java.library.path` AND
    + b) the `LD_LIBRARY_PATH` (linux) or `DYLD_LIBRARY_PATH` (OSX) or `PATH` (Windows).

Both items (2a) and (2b) above should be configured for reliable usage of the library. 

Z3 native libraries can either be taken directly from the `/lib/z3-x.x.x` directory of the repository or can be downloaded from the [Z3 Official Repository](https://github.com/z3prover). 

**NOTE 1:** In the future, it would be nice to have all of the Z3 native libraries some how packaged within standalone `jgt-deps-x.x.jar`. I have not figured out how to do this yet, any guidance/suggestions would be nice. 

**NOTE 2:** You may also use the `jgt-x.x.jar` however it does not contain some dependencies (e.g., `com.microsoft.z3.jar`) and so you might have to provide these additional dependencies manually. 


#### Development Setup

This project uses Maven to manage the build and testing. 

In addition to the setup listed above, you should also install two libraries into your local maven repository (`~/.m2`). There is a script (`scripts/install-dependencies.sh`) that can be used to do this. 

If you require additional guidance, take a look at the `.gitlab-ci.yml` file. This file contains the build commands used in the CI environment and should help get you started.


### Usage

See examples of using the jGT API in [jGT Samples repository](https://gitlab.com/sdiemert/gjt-samples).

See a detailed description of how to use jGT on the [project Wiki (TBD)](#).

